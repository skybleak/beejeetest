<?php use app\Helpers\UrlHelper;

require 'header.php' ?>
    <div class="container">
        <main>
            <h1>Список задач</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">
                        <span>Имя</span>
                        <p>
                            <a href="<?php echo UrlHelper::getLink('/', array(
                                'sort' => 'name',
                                'direction' => 'ASC'
                            )) ?>">По возрастанию</a><br><a href="<?php echo UrlHelper::getLink('/', array(
                                'sort' => 'name',
                                'direction' => 'DESC'
                            )) ?>">По убыванию</a>
                        </p>
                    </th>
                    <th scope="col">
                        <span>Email</span>
                        <p>
                            <a href="<?php echo UrlHelper::getLink('/', array(
                                'sort' => 'email',
                                'direction' => 'ASC'
                            )) ?>">По возрастанию</a><br><a href="<?php echo UrlHelper::getLink('/', array(
                                'sort' => 'email',
                                'direction' => 'DESC'
                            )) ?>">По убыванию</a>
                        </p>
                    </th>
                    <th scope="col">
                        <span>Описание задачи</span>
                        <p>
                            <a href="<?php echo UrlHelper::getLink('/', array(
                                'sort' => 'description',
                                'direction' => 'ASC'
                            )) ?>">По возрастанию</a><br><a href="
            <?php echo UrlHelper::getLink('/', array(
                                'sort' => 'description',
                                'direction' => 'DESC'
                            )) ?>">По убыванию</a>
                        </p>
                    </th>
                    <th scope="col">Статус</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($posts) && is_array($posts) && !empty($posts)): ?>
                    <?php foreach ($posts as $post): ?>
                        <tr>
                            <th scope="row"><?php echo $post['id'] ?></th>
                            <td><?php echo $post['name'] ?></td>
                            <td><?php echo $post['email'] ?></td>
                            <td><?php echo $post['description'] ?></td>
                            <td><?php echo ($post['status']) ? 'Выполнено' : 'Не выполнено' ?></td>
                            <td>
                                <?php if (isset($logged) && $logged): ?>
                                    <div class="col-md-1"><a href="/edit-post/?id=<?php echo $post['id'] ?>">Ред.</a>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a href="/add-post/" class="btn btn-success">Добавить задачу</a>
            <?php if (isset($logged) && $logged): ?>
                <a href="/user/logout/" class="btn btn-success">Выйти из аккаунта</a>
            <?php else: ?>
                <a href="/user/login/" class="btn btn-success">Войти в аккаунт</a>
            <?php endif; ?>
            <p aria-label="Page navigation example">
                <?php if (isset($count) && ceil($count / 3) > 1): ?>
            <ul class="pagination">
                <?php for ($i = 1; $i <= ceil($count / 3); $i++): ?>
                    <li class="page-item">
                        <a class="page-link"
                           href="<?php echo UrlHelper::getLink('/', array('page' => $i)) ?>"><?php echo $i ?></a>
                    </li>
                <?php endfor; ?>
            </ul>
        <?php endif; ?>
            </p>
        </main>
    </div>
<?php require 'footer.php' ?>