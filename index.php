<?php
require_once 'bootstrap.php';

use app\Controllers\MainController;
use app\Controllers\UserController;

use MiladRahimi\PhpRouter\Router;

$router = Router::create();

session_start();

$router->get('/', function () {
    return (new MainController())->index();
});
$router->get('/add-post/', function () {
    return (new MainController())->addPost();
});
$router->get('/edit-post/', function () {
    return (new MainController())->editPost();
});
$router->post('/save/', function () {
    return (new MainController())->save();
});
$router->get('/user/login/', function () {
    return (new UserController())->login();
});
$router->post('/user/login/', function () {
    return (new UserController())->login();
});
$router->get('/user/logout/', function () {
    return (new UserController())->logout();
});

$router->dispatch();


